(ns carp-todo-auth-backend.core
  (:gen-class)
  (:require
   [carp-todo-auth-backend.auth.context :as auth-context]
   [carp-todo-auth-backend.auth.crypt.hash.core :as auth-crypt]
   [carp-todo-auth-backend.auth.db.sql.core :as auth-db]
   [carp-todo-auth-backend.auth.web.core :as auth-web]
   [com.stuartsierra.component :as component]
   [environ.core :as environment]))

(def auth-system
  (component/system-map
   :config-options {:db {:jdbcUrl (environment/env :jdbcurl)}
                    :clj-env (environment/env :clj-env)
                    :port (-> :port environment/env Integer/parseUnsignedInt)
                    :redirect-uri (environment/env :redirect-uri)
                    :frontend-uri (environment/env :frontend-uri)
                    :client-id (environment/env :client-id)
                    :code-authorization-namespace (environment/env :code-authorization-namespace)
                    :token-secret (environment/env :token-secret)
                    :token-issuer (environment/env :token-issuer)}
   :crypt (auth-crypt/hash-crypt)
   :database (auth-db/sql-database)
   :context (auth-context/context)
   :web-server (auth-web/web-server)))

(defn -main
  "Start the authentication backend"
  [& _]
  (component/start auth-system))
