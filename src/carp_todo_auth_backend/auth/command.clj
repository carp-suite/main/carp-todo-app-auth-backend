(ns carp-todo-auth-backend.auth.command
  (:require
   [carp-todo-auth-backend.auth.crypt.core :as crypt]
   [carp-todo-auth-backend.auth.db.core :as db]))

(defmulti auth :type)

(defmethod auth :find-authorization-code
  [{:keys [db token]}]
  "Attempts to find authorization-code in database."
  (db/find-authorization-code db token))

(defmethod auth :generate-authorization-code
  [{:keys [hash challenge]}]
  "Generates authorization code from authorization challenge"
  (crypt/generate-authorization-code hash challenge))

(defmethod auth :is-code-challenge-valid?
  [{:keys [hash code-challenge code-verifier code-method]}]
  "Validates authorization code based on code verifier"
  (crypt/validate-code-verifier hash code-challenge code-verifier code-method))

(defmethod auth :find-user-id
  [{:keys [db user-secret]}]
  "Find user id based on provided user secret"
  (db/find-user-id db user-secret))

(defmethod auth :generate-token
  [{:keys [db hash user-id authorization-code]}]
  "Generate JWT token based on the user-id provided"
  (when-let [user-name (db/find-user-name db user-id)]
    (let [token (crypt/generate-token hash user-id user-name)]
      (db/register-token db token authorization-code user-id)
      token)))

(defmethod auth :find-token
  [{:keys [db authorization-code]}]
  "Find the already registered JWT token based on the authorization code provided"
  (db/find-token db authorization-code))

(defmethod auth :delete-token
  [{:keys [db user-id]}]
  "Find the already registered JWT token based on the authorization code provided"
  (db/delete-token db user-id))
