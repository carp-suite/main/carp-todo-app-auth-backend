(ns carp-todo-auth-backend.auth.core
  (:require
   [carp-todo-auth-backend.auth.command :as command]))

(defn- execute-command [context command]
  (command/auth (merge context command)))

(defn get-authorization-code [context token]
  (execute-command context {:type :find-authorization-code
                            :token token}))

(defn calculate-authorization-code [context challenge]
  (execute-command context {:type :generate-authorization-code
                            :challenge challenge}))

(defn is-code-challenge-valid? [context code-challenge code-verifier code-method]
  (execute-command context {:type :is-code-challenge-valid?
                            :code-challenge code-challenge
                            :code-verifier code-verifier
                            :code-method code-method}))

(defn find-user-id [context user-secret]
  (execute-command context {:type :find-user-id
                            :user-secret user-secret}))

(defn generate-token [context user-id authorization-code]
  (execute-command context {:type :generate-token
                            :user-id user-id
                            :authorization-code authorization-code}))

(defn find-token [context authorization-code]
  (execute-command context {:type :find-token
                            :authorization-code authorization-code}))

(defn delete-token [context user-id]
  (execute-command context {:type :find-token
                            :user-id user-id}))
