(ns carp-todo-auth-backend.auth.crypt.core)

(defprotocol Crypt
  "Deals with hashes, encryptions and tokens"
  (generate-token [this user-id user-name] "Generates an authorization token based on user's name and id")
  (generate-authorization-code [this code-challenge] "Generates an authorization code based on the client's challenge")
  (validate-code-verifier [this code-challenge code-verifier code-method] "Validates that the 'code-challenge' is the hash of the 'code-verifier' using method 'code-method'"))
