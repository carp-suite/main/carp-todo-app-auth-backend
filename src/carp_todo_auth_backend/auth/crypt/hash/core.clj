(ns carp-todo-auth-backend.auth.crypt.hash.core
  (:require
   [carp-todo-auth-backend.auth.crypt.core :as crypt]
   [clj-commons.digest :as digest]
   [clj-pkg.jwt :as jwt]
   [clj-uuid :as uuid]
   [com.stuartsierra.component :as component]
   [clojure.string :as string]))

(defrecord HashCrypt [config-options code-authorization-namespace token-issuer token-secret]
  crypt/Crypt

  (validate-code-verifier [_this code-challenge code-verifier code-method]
    (and (= code-method "S256")
         (-> code-verifier
             digest/sha-256
             string/lower-case
             (= (string/lower-case code-challenge)))))

  (generate-authorization-code [_this code-challenge]
    (->> code-challenge
         digest/md5
         (uuid/v5 code-authorization-namespace)))

  (generate-token [_this user-id user-name]
    (jwt/sign (jwt/jwt :hs256 {:sid user-id :iss token-issuer :name user-name}) token-secret))

  component/Lifecycle

  (start [this]
    (merge this (select-keys config-options [:code-authorization-namespace
                                             :token-secret
                                             :token-issuer])))

  (stop [this] this))

(defn hash-crypt []
  (component/using (map->HashCrypt {}) [:config-options]))
