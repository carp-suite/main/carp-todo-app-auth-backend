(ns carp-todo-auth-backend.auth.db.sql.core
  (:require
   [carp-todo-auth-backend.auth.db.core :as db]
   [clojure.tools.logging.readable :as log]
   [com.stuartsierra.component :as component]
   [next.jdbc.connection :as connection]
   [next.jdbc.plan :as plan]
   [next.jdbc :as jdbc])
  (:import
   [com.zaxxer.hikari HikariDataSource]))

(defrecord SqlRepository [config-options connection]
  db/Repository

  (find-authorization-code [_this token]
    (plan/select-one! connection :authorization-code ["SELECT authorization_code as \"authorization-code\" FROM registered_tokens WHERE token = ?" token]))

  (find-user-id [_this secret]
    (plan/select-one! connection :id ["SELECT id FROM users WHERE secret = ?" secret]))

  (find-user-name [_this id]
    (plan/select-one! connection :name ["SELECT name FROM users WHERE id = ?" id]))

  (register-token [_this token authorization-code user-id]
    (jdbc/execute-one! connection ["INSERT INTO registered_tokens (token, authorization_code, user_id) VALUES (?, ?, ?)" token authorization-code user-id]))

  (find-token [_this authorization-code]
    (plan/select-one! connection :token ["SELECT token from registered_tokens WHERE authorization_code = ?" authorization-code]))

  (delete-token [_this user-id]
    (jdbc/execute-one! connection ["DELETE FROM registered_tokens WHERE user_id = ?" user-id]))

  component/Lifecycle

  (start [this]
    (let [db-spec (:db config-options)
          connection (connection/->pool HikariDataSource db-spec)]
      (log/infof "Connecting to database %s" db-spec)
      (assoc this :connection connection)))

  (stop [_this]))

(defn sql-database
  "Start a connection to a database according to 'config-options'"
  []
  (component/using
   (map->SqlRepository {})
   [:config-options]))
