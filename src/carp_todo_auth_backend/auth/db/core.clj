(ns carp-todo-auth-backend.auth.db.core)

(defprotocol Repository
  "Looks up information related to the user and tokens"
  (find-authorization-code [this token] "Find the 'authorization code' associated with the provided token")
  (find-user-id [this secret] "Find the user's id associated with the user secret")
  (find-user-name [this id] "Find the user's name associated with the user id")
  (register-token [this token authorization-code user-id] "Register's the issued 'token' and associates it to an 'authorization code' and an 'user'")
  (find-token [this authorization-code] "Finds the token associated with the provided authorization code")
  (delete-token [this user-id] "Deletes the token associated with the provided user"))
