(ns carp-todo-auth-backend.auth.web.handlers
  (:require
   [carp-todo-auth-backend.auth.core :as auth]
   [ring.util.response :as response]))

(defn- parse-int [value]
  (try
    (Integer/parseInt value)
    (catch Exception _e
      nil)))

(defn authenticate
  "Attempts to authenticate the user by looking at the 'authorization' header
   if no tokens are found, return an HTML page requesting login"
  [req]
  (let [authorization-token (:authorization-token req)
        authorization-code (:authorization-code req)]
    (if (nil? authorization-token)
      (response/status 403)
      (response/response {:authorization_code authorization-code}))))

(defn interact
  "Confirms/rejects the authorization attempt. If confirmed, redirects to 'redirect_uri' with authorization code and state"
  [req]
  (let [authorization-code (:authorization-code req)
        cookies (:cookies req)
        params (:params req)
        component (:component req)
        {code-challenge :value} (get cookies "code_challenge")
        interaction-type (get params "type")
        user-secret (get params "user_secret")
        user-id (auth/find-user-id component user-secret)]
    (cond
      (or (= interaction-type "reject") (not code-challenge)) (response/status 403)
      (not user-id) (response/status 404)
      authorization-code (-> {:authorization_code authorization-code}
                             response/response
                             (response/set-cookie "user_id" user-id))
      (= interaction-type "confirm") (-> {:authorization_code (auth/calculate-authorization-code component code-challenge)}
                                         (response/response)
                                         (response/set-cookie "user_id" user-id))
      :else (response/status 403))))

(defn token
  "Returns the JWT related to this current user, if authenticated, else, returns a 401 response"
  [req]
  (let [params (:params req)
        component (:component req)
        code-verifier (get params "code_verifier")
        authorization-code (get params "code")
        cookies (:cookies req)
        {code-challenge :value} (get cookies "code_challenge")
        {code-challenge-method :value} (get cookies "code_challenge_method")
        {user-id-raw :value} (get cookies "user_id")
        user-id (parse-int user-id-raw)]
    (cond
      (not user-id) (response/bad-request "user_id must be integer")
      (not (auth/is-code-challenge-valid? component code-challenge code-verifier code-challenge-method)) (response/status 401)
      :else (if-let [token (or (auth/find-token component authorization-code) (auth/generate-token component user-id authorization-code))]
              (response/response token)
              (response/not-found (format "User not found with id %d" user-id))))))

(defn revoke
  "Deletes the JWT token related to this current user, if authenticated, else, returns a 401 response"
  [req]
  (let [component (:component req)
        cookies (:cookies req)
        {user-id-raw :value} (get cookies "user_id")
        user-id (parse-int user-id-raw)]
    (if (not user-id)
      (response/status 401)
      (do
        (auth/delete-token component user-id)
        (-> (response/status 200)
            (response/set-cookie "JWT" "" [:max-age 0]))))))
