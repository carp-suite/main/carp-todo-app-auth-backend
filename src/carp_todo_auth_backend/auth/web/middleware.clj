(ns carp-todo-auth-backend.auth.web.middleware
  (:require [carp-todo-auth-backend.auth.core :as auth]
            [ring.middleware.reload :as reload]
            [clojure.string :as string]
            [ring.util.response :as response]
            [clojure.tools.logging :as log]))

(defn wrap-config-options
  "Middleware that includes the configuration options of the application inside the request"
  [handler config-options]
  (fn [req]
    (handler (assoc req :config-options config-options))))

(defn wrap-component
  "Middleware that includes the database component inside the request"
  [handler component]
  (fn [req]
    (handler (assoc req :component component))))

(defn wrap-authenticated
  "Middleware that peaks at the 'Authorization' header and attaches the token to the request"
  [handler]
  (fn [req]
    (let [header-value (-> req :headers (get "authorization"))
          token        (if (-> header-value nil? not) (subs header-value 7) nil)
          component (:component req)
          code (auth/get-authorization-code component token)]
      (handler (if code
                 (-> req
                     (assoc :authorization-token token)
                     (assoc :authorization-code code))
                 req)))))

(defn wrap-scope-openid
  "Middleware that validates that the scope parameter is 'openid'"
  [handler]
  (fn [req]
    (if-let [scope (get (:params req) "scope")]
      (if (string/includes? scope "openid")
        (handler req)
        (response/bad-request "Scope should include 'openid'"))
      (handler req))))

(defn wrap-store-challenge
  "Middleware that stores the 'code_challenge' and 'code_challenge_method' in cookies"
  [handler]
  (fn [req]
    (let [params (:params req)
          code-challenge (get params "code_challenge")
          code-challenge-method (get params "code_challenge_method")]
      (if (and code-challenge code-challenge-method)
        (-> req
            handler
            (response/set-cookie "code_challenge" code-challenge)
            (response/set-cookie "code_challenge_method" code-challenge-method))
        (handler req)))))

(defn wrap-validate-response-type
  "Middleware that validates the 'response_type' parameter equals what was provided"
  [handler allowed-response-type]
  (fn [req]
    (if-let [response-type (get (:params req) "response_type")]
      (if (some #(= % response-type) allowed-response-type)
        (handler req)
        (response/bad-request (str "Response type should be one of: " (string/join ", " allowed-response-type))))
      (handler req))))

(defn wrap-reload-dev
  "Middleware that realoads the namespaces on change on development environment."
  [handler config]
  (let [env (:clj-env config)]
    (if (= env "development")
      (do
        (log/infof "Enabling reload middleware in environment %s" env)
        (reload/wrap-reload handler))
      handler)))

(defn wrap-redirect-uri-client-id-validation
  "Middleware that validates that 'redirect-uri' and 'client-id' match."
  [handler config]
  (fn [req]
    (let [params (:params req)
          redirect-uri (get params "redirect_uri")
          client-id (get params "client_id")]
      (cond
        (and (not redirect-uri) (not client-id)) (handler req)
        (and (= redirect-uri (:redirect-uri config)) (= client-id (:client-id config))) (handler req)
        :else (do
                (log/warnf "Redirect URI %s and Client ID %s do not match the stored values" redirect-uri client-id)
                (response/status 403))))))
