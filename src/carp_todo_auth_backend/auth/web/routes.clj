(ns carp-todo-auth-backend.auth.web.routes
  (:require
   [carp-todo-auth-backend.auth.web.handlers :as handlers]
   [compojure.core :refer [defroutes GET POST]]))

(defroutes routes
  (GET "/auth" req (handlers/authenticate req))
  (POST "/auth/interact" req (handlers/interact req))
  (POST "/auth/token" req (handlers/token req))
  (POST "/auth/revoke" req (handlers/revoke req)))
