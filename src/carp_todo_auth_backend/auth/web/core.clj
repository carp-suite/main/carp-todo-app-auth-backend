(ns carp-todo-auth-backend.auth.web.core
  (:require
   [carp-todo-auth-backend.auth.web.routes :as routes]
   [carp-todo-auth-backend.auth.web.middleware :as middleware]
   [clojure.tools.logging :as log]
   [com.stuartsierra.component :as component]
   [org.httpkit.server :as server]
   [ring.middleware.params :as ring]
   [ring.middleware.cookies :as cookies]
   [jumblerg.middleware.cors :as cors]
   [ring.middleware.json :as json]))

(defn- app-routes [context config]
  (-> routes/routes
      middleware/wrap-authenticated
      (middleware/wrap-reload-dev config)
      (middleware/wrap-component context)
      (middleware/wrap-config-options config)
      middleware/wrap-store-challenge
      middleware/wrap-scope-openid
      (middleware/wrap-validate-response-type ["code"])
      (middleware/wrap-redirect-uri-client-id-validation config)
      (ring/wrap-params {:encoding "UTF-8"})
      (cors/wrap-cors #".*")
      json/wrap-json-response
      cookies/wrap-cookies))

(defrecord WebServer [config-options context handle]
  component/Lifecycle

  (start [this]
    (log/infof "Starting server at http://0.0.0.0:%d" (:port config-options))
    (assoc this :handle (server/run-server (app-routes context config-options) {:port (:port config-options)})))
  (stop [_this]
    (when handle (handle :timeout 100))))

(defn web-server
  "Start a new http web server."
  []
  (component/using
   (map->WebServer {}) [:config-options :context]))
