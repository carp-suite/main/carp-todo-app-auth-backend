(ns carp-todo-auth-backend.auth.context
  (:require
   [com.stuartsierra.component :as component]))

(defrecord Context [db hash]
  component/Lifecycle

  (start [this] this)
  (stop [this] this))

(defn context []
  (component/using (map->Context {}) {:db :database
                                      :hash :crypt}))
