FROM openjdk:17 AS runtime
WORKDIR /carp-todo-auth/backend
COPY auth.db .
COPY target/uberjar/carp.todo_app.auth-1.0.0-standalone.jar .
COPY .lein-env .

CMD ["java", "-jar", "carp.todo_app.auth-1.0.0-standalone.jar"]
