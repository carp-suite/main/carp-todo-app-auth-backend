(defproject carp-todo-auth-backend "1.0.0"
  :description "Authentication service for the carp-todo-app project"
  :url "https://gitlab.com/carp-suite/main/carp-todo-app-auth-backend"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [compojure "1.7.0"]
                 [ring/ring-core "1.8.2"]
                 [ring/ring-devel "1.8.2"]
                 [ring/ring-jetty-adapter "1.8.2"]
                 [http-kit/http-kit "2.7.0"]
                 [com.stuartsierra/component "1.1.0"]
                 [org.clojure/core.async "1.6.681"]
                 [org.xerial/sqlite-jdbc "3.43.0.0"]
                 [org.clojure/java.jdbc "0.7.12"]
                 [com.github.seancorfield/next.jdbc "1.3.883"]
                 [org.clojure/tools.logging "1.2.4"]
                 [org.apache.logging.log4j/log4j-core "2.20.0"]
                 [hiccup "2.0.0-RC1"]
                 [migratus "1.5.3"]
                 [danlentz/clj-uuid "0.1.9"]
                 [environ "1.2.0"]
                 [org.clj-commons/digest "1.4.100"]
                 [clj-pkg/jwt "1.0.0"]
                 [com.zaxxer/HikariCP "3.3.1"]
                 [jumblerg/ring-cors "3.0.0"]
                 [ring/ring-json "0.5.1"]]
  :plugins [[migratus-lein "0.7.3"]
            [lein-environ "1.2.0"]]
  :jvm-opts ["-Dclojure.tools.logging.factory=clojure.tools.logging.impl/log4j2-factory"]
  :migratus {:store :database
             :migration-dir "resources/migrations/"
             :init-in-transaction? false
             :db {:dbtype "sqlite"
                  :classname "org.sqlite.JDBC"
                  :dbname "auth.db"}}
  :main ^:skip-aot carp-todo-auth-backend.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]
                       :name "carp.todo_app.auth"}
             :dev {:env {:clj-env "development"
                         :jdbcurl "jdbc:sqlite:./auth.db"
                         :frontend-uri "http://localhost:5173/auth/login"
                         :redirect-uri "http://localhost:5173/auth/callback"
                         :client-id "carp.todo_app.main"
                         :port 8080
                         :code-authorization-namespace "c85bc0e7-3dcc-46e8-b8a7-3c1dc0298cef"
                         :token-issuer "carp.todo_app.auth"
                         :token-secret "secret"}}
             :production {:env {:clj-env "production"
                                :jdbcurl "jdbc:sqlite:./auth.db"
                                :frontend-uri "http://carp.todo_app.ui/auth/login"
                                :redirect-uri "http://carp.todo_app.ui/auth/callback"
                                :client-id "carp.todo_app.main"
                                :port 80
                                :code-authorization-namespace "c85bc0e7-3dcc-46e8-b8a7-3c1dc0298cef"
                                :token-issuer "carp.todo_app.auth"
                                :token-secret "secret"}}
             :test {:migratus {:store :database
                               :migration-dir "resources/migrations/"
                               :init-in-transaction? false
                               :db {:dbtype "sqlite"
                                    :classname "org.sqlite.JDBC"
                                    :dbname "auth.test.db"}}
                    :dependencies [[clj-http "3.12.3"]
                                   [lambdaisland/uri "1.16.134"]]
                    :env {:clj-env "testing"
                          :jdbcurl "jdbc:sqlite:./auth.test.db"
                          :frontend-uri "http://frontend.test.uri"
                          :redirect-uri "http://test.uri"
                          :client-id "test"
                          :port 5500
                          :code-authorization-namespace "c85bc0e7-3dcc-46e8-b8a7-3c1dc0298cef"
                          :token-issuer "carp.todo_app.auth"
                          :token-secret "secret"}}})
