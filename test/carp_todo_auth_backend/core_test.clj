(ns carp-todo-auth-backend.core-test
  (:require
   [carp-todo-auth-backend.core :as core]
   [clj-commons.digest :as digest]
   [clj-http.client :as client]
   [clj-pkg.jwt :as jwt]
   [clj-uuid :as uuid]
   [clojure.test :as test]
   [com.stuartsierra.component :as component]
   [next.jdbc :as jdbc]
   [clojure.data.json :as json]))

(test/deftest authorization-endpoint-test
  (let [system (component/start core/auth-system) config-options (:config-options system) system-url (format "http://localhost:%d/auth" (:port config-options)) db-spec (:db config-options)
        redirect-uri (:redirect-uri config-options)]
    (try
      (test/testing "Returns 'Unauthorized' if authorization fails"
        (let [auth-response (client/get system-url {:redirect-strategy :none
                                                    :throw-exceptions false
                                                    :trace-redirects true
                                                    :query-params {"code_challenge" "7462913068792DC9B358C457CA59234E7FB0620173D858A994D335F28F54F801"
                                                                   "code_method" "S256"
                                                                   "scope" "openid"
                                                                   "redirect_uri" redirect-uri
                                                                   "client_id" (:client-id config-options)}})]
          (test/is (= 403 (:status auth-response)))))
      (test/testing "Returns 'OK' with authorization code if authorization is present and valid"
        (try
          (jdbc/execute-one! db-spec ["INSERT INTO registered_tokens (token, authorization_code) VALUES (?, ?)" "12" "13"])
          (let [auth-response (client/get system-url {:redirect-strategy :none
                                                      :trace-redirects true
                                                      :query-params {"scope" "openid"
                                                                     "redirect_uri" redirect-uri
                                                                     "client_id" (:client-id config-options)
                                                                     "code_challenge" "7462913068792DC9B358C457CA59234E7FB0620173D858A994D335F28F54F801"
                                                                     "code_method" "S256"
                                                                     "state" 456}
                                                      :headers {"authorization" "Bearer 12"}})]
            (test/is (= 200 (:status auth-response)))
            (test/is (= {"authorization-code" "13"} (-> auth-response :body json/read-str))))
          (finally
            (jdbc/execute-one! db-spec ["DELETE FROM registered_tokens WHERE token = ?" "12"]))))
      (finally
        (component/stop system)))))

(test/deftest interact-endpoint-test
  (let [system (component/start core/auth-system)
        config-options (:config-options system)
        system-url (format "http://localhost:%d/interact" (:port config-options))
        db-spec (:db config-options)]
    (try
      (test/testing "Returns 'unauthorized' if interaction is rejected"
        (let [auth-response (client/post system-url {:redirect-strategy :none
                                                     :throw-exceptions false
                                                     :query-params {"type" "reject"
                                                                    "redirect_uri" (:redirect-uri config-options)
                                                                    "client_id" (:client-id config-options)}})]
          (test/is (= 403 (:status auth-response)))))
      (test/testing "Returns 'not found' if user cannot be found"
        (let [auth-response (client/post system-url {:redirect-strategy :none
                                                     :throw-exceptions false
                                                     :query-params {"type" "confirm"
                                                                    "redirect_uri" (:redirect-uri config-options)
                                                                    "client_id" (:client-id config-options)
                                                                    "user_secret" "123"}
                                                     :cookies {"code_challenge" {:path "/"
                                                                                 :value "7462913068792DC9B358C457CA59234E7FB0620173D858A994D335F28F54F801"}}})]
          (test/is (= 404 (:status auth-response)))))
      (test/testing "Returns 'OK' with authorization code if user is be found"
        (try
          (jdbc/execute-one! db-spec ["INSERT INTO users (id, name, secret) VALUES (?, ?, ?)" 1 "Michael Nelo" "secret"])
          (let [code-challenge "7462913068792DC9B358C457CA59234E7FB0620173D858A994D335F28F54F801"
                code-authorization (->> code-challenge digest/md5 (uuid/v5 (:code-authorization-namespace config-options)))
                auth-response (client/post system-url {:redirect-strategy :none
                                                       :throw-exceptions false
                                                       :query-params {"type" "confirm"
                                                                      "redirect_uri" (:redirect-uri config-options)
                                                                      "client_id" (:client-id config-options)
                                                                      "user_secret" "secret"}
                                                       :cookies {"code_challenge" {:path "/"
                                                                                   :value code-challenge}}})]
            (test/is (= 200 (:status auth-response)))
            (test/is (= {"authorization-code" (uuid/to-string code-authorization)} (-> auth-response :body json/read-str))))
          (finally
            (jdbc/execute-one! db-spec ["DELETE FROM users WHERE id = ?" 1]))))
      (finally
        (component/stop system)))))

(test/deftest token-endpoint-test
  (let [system (component/start core/auth-system)
        code-challenge "7462913068792DC9B358C457CA59234E7FB0620173D858A994D335F28F54F801"
        config-options (:config-options system)
        system-url (format "http://localhost:%d/token" (:port config-options))
        db-spec (:db config-options)]
    (try
      (test/testing "Returns 'unauthorized' if stored 'code_challenge' and 'code_challenge_method' do not match 'code_verifier'"
        (let [auth-response (client/post system-url {:redirect-strategy :none
                                                     :throw-exceptions false
                                                     :query-params {"code_verifier" "123"}
                                                     :cookies {"code_challenge" {:path "/"
                                                                                 :value code-challenge}
                                                               "code_method" {:path "/"
                                                                              :value "S256"}
                                                               "user_id" {:path "/"
                                                                          :value "256"}}})]
          (test/is (= 401 (:status auth-response)))))
      (test/testing "Returns JWT token if stored 'code_challenge' and 'code_challenge_method' match 'code_verifier'"
        (try
          (jdbc/execute-one! db-spec ["INSERT INTO users (id, name, secret) VALUES (?, ?, ?)" 1 "Michael Nelo" "secret"])
          (let [auth-response (client/post system-url {:redirect-strategy :none
                                                       :throw-exceptions false
                                                       :query-params {"code_verifier" "Hello World"}
                                                       :cookies {"code_challenge" {:path "/"
                                                                                   :value "A591A6D40BF420404A011733CFB7B190D62C65BF0BCDA32B57B277D9AD9F146E"}
                                                                 "code_challenge_method" {:path "/"
                                                                                          :value "S256"}
                                                                 "user_id" {:path "/"
                                                                            :value "1"}}})]
            (try
              (test/is (= 200 (:status auth-response)))
              (test/is (= 1
                          (-> (:body auth-response)
                              jwt/str->jwt
                              :claims
                              :sid)))
              (test/is (= "Michael Nelo"
                          (-> (:body auth-response)
                              jwt/str->jwt
                              :claims
                              :name)))
              (test/is (= (:token-issuer config-options)
                          (-> (:body auth-response)
                              jwt/str->jwt
                              :claims
                              :iss)))
              (finally
                (jdbc/execute-one! db-spec ["DELETE FROM registered_tokens WHERE token = ?" (:body auth-response)]))))
          (finally
            (jdbc/execute-one! db-spec ["DELETE FROM users WHERE name = ?" "Michael Nelo"]))))
      (finally
        (component/stop system)))))
