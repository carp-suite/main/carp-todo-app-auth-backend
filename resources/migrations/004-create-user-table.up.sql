CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  secret BLOB(16) NOT NULL
)
--;;
CREATE INDEX idx_users_secret
ON users(secret);
