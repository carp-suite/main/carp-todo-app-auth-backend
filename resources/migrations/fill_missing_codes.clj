(ns migrations.fill-missing-codes
  (:require
   [environ.core :as environment]
   [clj-uuid :as uuid]
   [next.jdbc :as jdbc]))

#_{:clj-kondo/ignore [:clojure-lsp/unused-public-var]}
(defn migrate-up [config]
  (let [conn (-> config :conn)]
    (->> (jdbc/plan conn ["SELECT token FROM registered_tokens WHERE authorization_code IS NULL"])
         (run! #(jdbc/execute! conn ["UPDATE registered_tokens SET authorization_code = ? WHERE token = ?" (uuid/v5 (environment/env :authorization-namespace) (:token %)) (:token %)])))))
